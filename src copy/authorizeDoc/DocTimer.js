import React from 'react';

class Timer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      time: new Date().toLocaleTimeString(),
    }
  }
  componentDidMount() {
    this.timerId = setInterval(() => {
      this.setState({
        time: new Date().toLocaleTimeString()
      })
    }, 1000)
  }
  componentWillUnmount() {
    clearInterval(this.timerId)
  }

  render() {
    return (
        <div>
          <h1>Hello, world!</h1>
          <h2>It is {this.state.time}.</h2>
        </div>
    );
  }
}
export default Timer