import './TestChild.css'

function Till({children}) {
  return (
    <div className="tilt-root">
      <div className="tilt-child">{children}</div>
    </div>
  )
}

function App() {
  return (
    <Till>
      <div className="totally-centered">child.js</div>
    </Till>
  )
}

export default App