import React from 'react'

function BoilingVerdict(props) {
  let text;
  if(props.temp < 100) {
    text = 'the water would be boil'
  } else {
    text = 'the water is boiling'
  }
  return (
      <p>{text}</p>
  )
}

let scaleNames = {
  c: 'Celsius',
  f: 'Fahrenheit'
}
class TemperatureInput extends React.Component {
  handleChange(e) {

  }

  render() {
    let temperature = this.props.temp;
    return (
      <fieldset>
        <legend>Enter temperature in {scaleNames[this.props.type]}:</legend>
        <input
            value={temperature}
            onChange={(e) => this.props.onChange(e, this.props.type)} />
      </fieldset>
    )
  }
}

function toCelsius(temperature) {
  return (temperature - 32) * 5 / 9
}

function toFahrenheit(temperature) {
  return (temperature * 9 / 5) + 32;
}
class Calculator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      temperature: '',
      scale: ''
    }
    this.handleChange = this.handleChange.bind(this)
  }
  handleChange(e, type) {
    let value = parseFloat(e.target.value);
    if(Number.isNaN(value)) {
      return
    }
    // const scale = this.state.scale;
    this.setState({
      temperature: this.state.temperature,
      scale: type
    })
  }

  render() {
    const temperature = this.state.temperature;
    const scale = this.state.scale;
    return (
      <div>
        <TemperatureInput type='c' onChange={this.handleChangeC} temp={scale == 'c' ? temperature : toCelsius(temperature)}/>
        <TemperatureInput type='f' onChange={this.handleChangeF} temp={scale == 'f' ? temperature : toFahrenheit(temperature)}/>
        <BoilingVerdict temp={this.state.c}/>
      </div>
    )
  }
}
export default Calculator