import React from 'react';
import Timer from './DocTimer'
import Temperature from './Temperature'
import TestChild from './TestChild'

export default function Doc() {
  return (
      <div>
        <Timer />
        <Temperature />
        <TestChild />
      </div>
  )
}