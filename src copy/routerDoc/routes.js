import React from 'react';
import {Router, Route, IndexRoute} from 'react-router'
import * as history from 'history'

class App extends React.Component {
    render() {
        return (
            <div>
                <p>App</p>
                <ul>
                    <li><a href="/about" >about</a></li>
                    <li><a href="/inbox" >inbox</a></li>
                </ul>
                {this.props.children}
            </div>
        )
    }
}

function About(props) {
    return (
        <a href="#">this is an about href</a>
    )
}

function Inbox(props) {
    console.log(props.children)
    return (
        <a href="/inbox/message/123">this is an inbox href {props.children || 'welcome to inbox'}</a>
    )
}

function Message(props) {
    console.log(props)
    return (
        <a href="#">this is an message href with {props.params.id}</a>
    )
}
export default function MyRouter() {
    return (
        <Router history={history.createHistory()}>
            <Route path="/" component={App} name="App">
                <IndexRoute component={About} name="about"/>
                <Route path="inbox" component={Inbox} name="inbox" >
                    <Route path="message/:id" component={Message} name="message" />
                </Route>
                <Route path="/about" component={About} name="about"/>
            </Route>
        </Router>
    )
}