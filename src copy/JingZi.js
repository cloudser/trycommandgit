import React from 'react';

function Square(props) {
  console.log(props)
  return (
    <button className="square" onClick={props.onClick}>
      {props.value}
    </button>
  )
}

class Board extends React.Component{
  renderSquare(i) {
    return (
        <Square value={this.props.keys[i]}
                onClick={() => {this.props.onClick(i)}}
        />
    )
  }

  render() {
    return (
        <div>
          <div className="board-row">
            {this.renderSquare(0)}
            {this.renderSquare(1)}
            {this.renderSquare(2)}
          </div>
          <div className="board-row">
            {this.renderSquare(3)}
            {this.renderSquare(4)}
            {this.renderSquare(5)}
          </div>
          <div className="board-row">
            {this.renderSquare(6)}
            {this.renderSquare(7)}
            {this.renderSquare(8)}
          </div>
        </div>
    )
  }
}

export default class JingZi extends React.Component{
  constructor(props) {
    super(props)
    this.state = {
      history: [{
        squares: Array(9).fill(null),
      }],
      stepNumber: 0,
      xIsNext: true
    }
  }
  handleClick(i) { //处理事件的监听方法
    //需要创建状态的副本，保持不可变性
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const keys = history[history.length - 1].squares.slice();
    if(this.calculatorWinner(keys) || keys[i]) {
      return
    }
    keys[i] = !this.state.xIsNext ? 'O' : 'X';
    this.setState({
      history: history.concat({
        squares: keys
      }),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext
    })
  }
  calculatorWinner(square) {
    const line = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    for(let i = 0; i < line.length; i++) {
      const [a, b, c] = line[i]
      if(square[a] && square[a] === square[b] && square[a] === square[c]) {
        return square[a]
      }
    }
    return null
  }
  jumpTo(move) {
    this.setState({
      // history: this.state.history.slice(0, move + 1),
      stepNumber: move,
      xIsNext: (move % 2) === 0
    })
  }

  render() {
    const length = this.state.history.length;
    const keys = this.state.history[this.state.stepNumber].squares;
    const result = this.calculatorWinner(keys);
    let moves = this.state.history.map((step, move) => {
      const text = move ?
          'Go to move #' + move :
          'Go to game start';
      return (
          <li key={move}>
            <button onClick={() => this.jumpTo(move)}>{text}</button>
          </li>
      )
    })
    let status;
    if(result) {
      status = 'Winner is: ' + result;
    } else {
      status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
    }

    return (
        <div className="game">
          <div className="game-board">
            <Board keys={keys} onClick={i => this.handleClick(i)}/>
          </div>
          <div className="game-info">
            <div>{status}</div>
            <ol>{moves}</ol>
          </div>
        </div>
    )
  }
}